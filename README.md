CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Mapbox gl location picker. By now it adds just the picker as a geofield
proximity source plugin, so it can be used in geofield views filters.

It relies on mapbox_gl module and the geofield module.


REQUIREMENTS
------------

This module requires the following modules:
  * Geofield - (https://www.drupal.org/project/geofield)
  * Mapbox GL - (https://www.drupal.org/project/mapbox_gl)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, it will show an location marker on the map. The latitude and longitudes
are provided by the Geofield module.


MAINTAINERS
-----------

Current maintainer:
  Aleix Quintana (aleix) -  https://www.drupal.org/u/aleix

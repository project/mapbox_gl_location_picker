(function (Drupal, $, once) {
  Drupal.behaviors.mapbox_gl_location_picker = {
    attach: function (context, settings) {
      if (!(context === window.document)) {
        return;
      }
      // the array that controls which map is already set to prevent do it
      // twice if new-layer event comes from map rather than document would help
      // but it is coming from root document. Not really used...
      maps_set = [];
      var lat_input = [];
      var lon_input = [];
      var radius_dist = [];
      var metric_conversion_factor = [];
      var text_input = [];
      var distance_feature_collection = [];
      var on_configuration = [];


      for (const picker_key in settings.mapbox_gl_location_picker ) {
        var picker = settings.mapbox_gl_location_picker[picker_key];
        // mandatory input form elements:
        lat_input[picker_key] = "";
        lon_input[picker_key] = "";
        // optional input form element:
        //   radius_disti
        radius_dist[picker_key] = "";
        metric_conversion_factor[picker_key] = "";
        // optional iext input element to allow pick with polygon, line, etc.
        // @TODO
        text_input[picker_key] = "";

        //used to update paint features of the distance radius
        distance_feature_collection[picker_key] = [];



        if (once("mapbox_gl_location_picker-" + picker_key, 'html').length) {
          $(document).on(
            'new-layer', function (event, map, mapData, mapId, index, layerData) {
              if (/*(maps_set.indexOf(mapId) > -1) ||*/ ( mapId != settings.mapbox_gl_location_picker[picker_key].map_id ) ){
                // It will exit if map is already set or if mapid is not for this
                // picker
                return;
              }
              // disabling as it will be used to locate the marker
              map.doubleClickZoom.disable();
              // set the field identifiers to pass modifications on picker changes
              // or viceversa
              // Ideally it may come from settings, but often it must be obtained
              // via DOM selectors-
              // We need: lat lon and optionally radius_dist and text input.
              switch (settings.mapbox_gl_location_picker[picker_key].picker_type){
                case "base_form":
                case "widget":
                  // todo.
                case "views_filter":
                  // set necessary context
                  let views_filter_context = map._container.closest("fieldset");
                  // The radius dist element we can use
                  radius_dist[picker_key] = $(views_filter_context).find("[name=\"" + settings.mapbox_gl_location_picker[picker_key].views_handler_id + "[value]\"]");
                  metric_conversion_factor[picker_key] = settings.mapbox_gl_location_picker[picker_key].metric_conversion_factor;
                  // The coordinates form input elements
                  lat_input[picker_key] = $(views_filter_context).find("[name=\"" + settings.mapbox_gl_location_picker[picker_key].views_handler_id + "[source_configuration][origin][lat]\"")
                  lon_input[picker_key] = $(views_filter_context).find("[name=\"" + settings.mapbox_gl_location_picker[picker_key].views_handler_id + "[source_configuration][origin][lon]\"")
                  // if the form is in configuration rather than exposed
                  on_configuration[picker_key] = $(context).find("#edit-instance").length;

                  // The distance feature collection in its layer radius_distance
                  distance_feature_collection[picker_key] = map.getSource("radius_distance").serialize().data;

                  $(views_filter_context).find("[name=\"clear-button\"]").click( function(){
                    lat_input[picker_key].val(0);
                    lon_input[picker_key].val(0);
                    if (radius_dist[picker_key]){
                      radius_dist[picker_key].val(0);
                    }
                    lat_input[picker_key].trigger("change");
                    radius_dist[picker_key].trigger("change");
                    $(views_filter_context).find(".mapboxgl-marker").hide();
                  });

                  break;
              }

              // Pointing operations:
              map.loadImage(
                settings.mapbox_gl_location_picker[picker_key].marker_image, function (error, image) {
                  if (error) { throw error;
                  }
                  map.addImage('marker_image', image);
                }
              );

              layerData.source.data.features.forEach(
                function (point) {
                  if (settings.mapbox_gl_location_picker[picker_key].marker_style == "marker" && layerData.id == "points"){
                    lon_input_val = (lon_input[picker_key].val())?lon_input[picker_key].val():"0.0";
                    lat_input_val = (lat_input[picker_key].val())?lat_input[picker_key].val():"0.0";
                    switch (settings.mapbox_gl_location_picker[picker_key].picker_type){
                      case "widget":
                      case "views_filter":
                        // Add some controls
                        var geolocate = new mapboxgl.GeolocateControl({
                          showUserLocation: false
                        });
                        map.addControl(
                          new MapboxGeocoder({
                            accessToken: mapboxgl.accessToken,
                            mapboxgl: mapboxgl,
                            marker: false
                          })
                        );
                        map.addControl(geolocate);
                        // Add marker
                        //TODO if circle or marker please:
                        var marker = new mapboxgl.Marker({
                          draggable: true
                        })
                          .setLngLat([lon_input_val, lat_input_val])
                          .addTo(map);
                        // will use coordinates from input instead of pointing to
                        // configurated data from
                        // point['geometry'].coordinates

                        // distance radius point
                        distance_feature_collection[picker_key].features[0].geometry.coordinates = [lat_input[picker_key].val(), lon_input[picker_key].val()]
                        map.getSource("radius_distance").setData(distance_feature_collection[picker_key])

                        if (on_configuration[picker_key]){
                          //To modify behavior if we are in configuration
                        }

                        marker.on('dragend', onDragEnd);
                        geolocate.on('geolocate', onGeolocate);
                        marker.on('drag', onDrag);
                        marker.on('move',onDrag);
                        map.on('dblclick', onMapDblClick);

                        if (radius_dist[picker_key]){
                          radius_dist[picker_key].on('change', updateRadius);
                          // initialize radius on map simulating drag and change
                          radius_dist[picker_key].trigger("change")
                          marker.fire("drag")
                        }

                        function onMapDblClick(data){
                          $(map._container.closest("fieldset")).find(".mapboxgl-marker").show();
                          marker.setLngLat([data.lngLat.lng,data.lngLat.lat]);
                          // fire drag to carry on its bindings
                          marker.fire("drag");
                          marker.fire("dragend");
                        }

                        function updateRadius(data){
                          // Update the radius dist visually
                          current_latitude = marker.getLngLat().lat;
                          var radiusData = {
                            stops: [
                              [0, 0],
                              [22, metersToPixelsAtMaxZoom(metric_conversion_factor[picker_key]*this.value, current_latitude)]
                            ],
                            base: 2
                          };
                          map.setPaintProperty('radius_distance','circle-radius', radiusData);
                        }
                        function onGeolocate(position){
                          marker.setLngLat([position.coords.longitude,position.coords.latitude]);
                          // fire drag to carry on its bindings
                          marker.fire("drag");
                          marker.fire("dragend");
                        }
                        function onDrag(data){
                          // Feedback to Radius distance layer point to mirror
                          // coords
                          distance_feature_collection[picker_key].features[0].geometry.coordinates = [data.target.getLngLat().lng, data.target.getLngLat().lat]
                          map.getSource("radius_distance").setData(distance_feature_collection[picker_key])
                        }
                        function onDragEnd() {
                          // Populate Coords input
                          var lngLat = marker.getLngLat();
                          //'Longitude: ' + lngLat.lng + '<br />Latitude: ' + lngLat.lat;
                          updateLocation(lngLat,settings.mapbox_gl_location_picker[picker_key].picker_type);
                        }
                        function updateLocation(position, picker_type) {
                          if (!on_configuration[picker_key]){
                            lat_input[picker_key].val(position.lat);
                            lat_input[picker_key].trigger("change");
                            lon_input[picker_key].val(position.lng);
                            lon_input[picker_key].trigger("change");
                          }
                        }
                        break;
                      case "field_formatter":
                        var marker = new mapboxgl.Marker({})
                          .setLngLat(point['geometry'].coordinates)
                          .setPopup(
                            new mapboxgl.Popup({ offset: 25 })
                            .setHTML(
                              '<h2><a href="' +point.properties.geourl+ '" target="_blank">Go there</a></h2>' +
                              point.properties.description
                            )
                          )
                          .addTo(map)
                        break;
                    }
                  }
                }
              )
              map.on(
                'click', 'points', function (e) {
                  var coordinates = e.features[0].geometry.coordinates.slice();
                  // Ensure that if the map is zoomed out such that multiple
                  // copies of the feature are visible, the popup appears
                  // over the copy being pointed to.
                  while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                  }
                  new mapboxgl.Popup()
                    .setLngLat(coordinates)
                    .addTo(map);
                  debugger
                }
              )
              maps_set.push(mapId);
            }
          );
        }
      }
      // On new layer
      $(document).on(
        'new-layer', function (event, map, mapData, mapId, index, layerData) {
        }
      );

      // On new source
      $(document).on(
        'new-source', function (event, map,mapData, mapId, sourceId, sourceData) {
          console.log(sourceData)
        }
      );

      // On new map
      $(document).on(
        "mapbox-gl.map", function ( event, map, mapData, mapId ) {
          map.on(
            'data', function (data) {
              if (data.dataType === 'source' && data.isSourceLoaded) {
              }
            }
          )
        }
      );


      function metersToPixelsAtMaxZoom(meters, latitude){
        // ~0.075m/px (meters per pixel)
        return (meters / 0.075 / Math.cos(latitude * Math.PI / 180)) * 4
      }
      function convertUnits(){
      }

      $("")


    }
  };
})(Drupal, jQuery, once);

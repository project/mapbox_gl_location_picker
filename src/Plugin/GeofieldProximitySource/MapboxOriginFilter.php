<?php

namespace Drupal\mapbox_gl_location_picker\Plugin\GeofieldProximitySource;

use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield\Plugin\GeofieldProximitySourceBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\mapbox_gl_location_picker\MapboxMapHandlerTrait;

/**
 * Defines 'Geofield Mapbox Origin' plugin.
 *
 * @package Drupal\geofield\Plugin
 *
 * @GeofieldProximitySource(
 *   id = "mapbox_origin",
 *   label = @Translation("Mapbox Origin"),
 *   description = @Translation("Origin."),
 *   exposedDescription = @Translation("Input of Origin (as couple of Latitude and Longitude in decimal degrees.)"),
 *   context = {
 *   },
 * )
 */
class MapboxOriginFilter extends GeofieldProximitySourceBase {

  use MapboxMapHandlerTrait;

  /**
   * The map array to feed mapbox.
   *
   * @var array
   */
  protected $map;

  /**
   * Constructs a ManualOriginDefault object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    //Origin must be numeric, if not an exception will raise:
    //Cannot construct Point. x and y should be numeric
    $this->origin['lat'] = isset($configuration['origin']) && is_numeric($configuration['origin']['lat']) ? $configuration['origin']['lat'] : 0;
    $this->origin['lon'] = isset($configuration['origin']) && is_numeric($configuration['origin']['lon']) ? $configuration['origin']['lon'] : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(array &$form, FormStateInterface $form_state, array $options_parents, $is_exposed = FALSE) {
    if (!$is_exposed){
      $form['help_text'] = [
        '#title' => $this->t('Help and tips'),
        '#type' => 'textarea',
        '#description' => $this->t('Text to help on how to apply this filter.'),
        '#default_value' => isset($this->configuration['help_text']) ? $this->configuration['help_text'] : FALSE,
      ];
      $form['mapbox_api_key'] = [
        '#title' => $this->t('Mapbox api key'),
        '#type' => 'textfield',
        '#default_value' => isset($this->configuration['mapbox_api_key']) ? $this->configuration['mapbox_api_key'] : FALSE,
      ];
      $form['style'] = [
        '#title' => $this->t('Style'),
        '#type' => 'textfield',
        '#default_value' => isset($this->configuration['style']) ? $this->configuration['style'] : FALSE,
        '#required' => true,
      ];
      $form['zoom'] = [
        '#type' => 'number',
        '#min' => 0,
        '#max' => 20,
        '#title' => $this->t('Map zoom'),
        '#default_value' => isset($this->configuration['zoom']) ? $this->configuration['zoom'] : 10,
      ];
      $form['marker_style'] = [
        '#type' => 'select',
        '#title' => $this->t('Marker style'),
        '#default_value' => isset($this->configuration['marker_style']) ? $this->configuration['marker_style'] : "marker",
        '#options' => [
          'marker' => 'Marker',
          // TODO enable these markers in js side:
        /*'circle' => 'Circle',
        'symbol' => 'Symbol',*/
        ]
      ];
      $form['circle_radius'] = [
        '#type' => 'number',
        '#min' => 0,
        '#title' => $this->t('Circle radius'),
        '#default_value' => isset($this->configuration['circle_radius']) ? $this->configuration['circle_radius'] : FALSE,
        '#states' => [
          'visible' => [
            "select[name=\"options[source_configuration][marker_style]\"]" => ['value' => 'circle'],
          ]
        ]
      ];
      $form['circle_color'] = [
        '#type' => 'color',
        '#default_value' => isset($this->configuration['circle_color']) ? $this->configuration['circle_color'] : FALSE,
        '#title' => $this->t('Circle color'),
        '#states' => [
          'visible' => [
            "select[name=\"options[source_configuration][marker_style]\"]" => ['value' => 'circle'],
          ]
        ]
      ];
    }

    if ($is_exposed){
      $form["help"] = [
        '#markup' => $this->t($this->configuration['help_text']),
      ];
      $form["map"] = $this->mapPicker($form, $form_state);
      $form["clear_button"] = [
        '#type' => 'button',
        '#name' => 'clear-button',
        '#access' => $this->configuration['show_clear_button'],
        '#pre_render' => '',
        '#attributes' => [
          "type" => "button",
          "name" => "clear-button",
          "value" => t('Clear'),
        ]
      ];
    }

    $form["origin"] = [
      '#title' => $this->t('Origin Coordinates'),
      '#type' => 'geofield_latlon',
      '#description' => $this->t('Value in decimal degrees. Use dot (.) as decimal separator.'),
      '#default_value' => [
        'lat' => $this->origin['lat'],
        'lon' => $this->origin['lon'],
      ],
    ];

    if (!$is_exposed) {
      $form["center"] = [
        '#title' => $this->t('Center location'),
        '#type' => 'geofield_latlon',
        '#description' => $this->t('Value in decimal degrees. Use dot (.) as decimal separator.'),
        '#default_value' => [
          'lat' => isset($this->configuration["center"]["lat"])?$this->configuration["center"]["lat"]:0,
          'lon' => isset($this->configuration["center"]["lon"])?$this->configuration["center"]["lon"]:0,
        ],
      ];

      $form['show_clear_button'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Show a clear button to set items to not be included in query'),
        '#default_value' => isset($this->configuration['show_clear_button']) ? $this->configuration['show_clear_button'] : TRUE,
      ];

      // If it is a proximity filter context
      // $this->viewHandler->configuration['id'] == 'geofield_proximity_filter'
      // and IS NOT exposed, render origin
      // hidden and origin_summary options.

      $form['origin_hidden_flag'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Hide the Origin Input elements from the Exposed Form'),
        '#default_value' => isset($this->configuration['origin_hidden_flag']) ? $this->configuration['origin_hidden_flag'] : FALSE,
        '#states' => [
          'visible' => [
            ':input[name="options[expose_button][checkbox][checkbox]"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form['origin_summary_flag'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Show (anyway) the Origin coordinates as summary in the Exposed Form'),
        '#default_value' => isset($this->configuration['origin_summary_flag']) ? $this->configuration['origin_summary_flag'] : TRUE,
        '#states' => [
          'visible' => [
            ':input[name="options[source_configuration][origin_hidden_flag]"]' => ['checked' => TRUE],
          ],
        ],
      ];

    }

    // If it IS exposed, eventually Hide the Origin components..
    if ($is_exposed && (isset($this->configuration['origin_hidden_flag']) && $this->configuration['origin_hidden_flag'])) {

      $form["origin"]['#attributes']['class'][] = 'visually-hidden';

      // Eventually Render the Origin Summary.
      if (isset($this->configuration['origin_summary_flag']) && $this->configuration['origin_summary_flag']) {
        $summary_lat = $this->origin['lat'];
        $summary_lon = $this->origin['lon'];
        $summary_distance = $this->viewHandler->options["value"];
        //$summary_units = geofield_radius_options()[$this->viewHandler->options['units']];
        if (isset($form_state->getUserInput()[$this->viewHandler->options['id']]['source_configuration']['origin']['lat'])){
          $source_input = $form_state->getUserInput()[$this->viewHandler->options['id']]['source_configuration'];
          $summary_lat = round($source_input['origin']['lat'], 2);
          $summary_lon = round($source_input['origin']['lon'], 2);
          $summary_distance = $form_state->getUserInput()[$this->viewHandler->options['id']]['value'];
        }
        // Show only if distance is set
        if ($form_state->getUserInput()[$this->viewHandler->options['id']]['value'] != "") {
          $form['origin_summary'] = [
            "#type" => 'html_tag',
            "#tag" => 'div',
            '#value' => $this->t('@dist @units @operator Latitude: @lat and Longitude: @lon.', [
              '@dist' => $summary_distance,
              '@operator' => $this->viewHandler->options['operator'],
              '@units' => geofield_radius_options()[$this->viewHandler->options['units']],
              '@lat' => new FormattableMarkup('<span class="geofield-lat"> @lat</span>', [
                '@lat' => $summary_lat,
              ]),
              '@lon' => new FormattableMarkup('<span class="geofield-lon"> @lon</span>', [
                '@lon' => $summary_lon,
              ]),
            ]),
            '#attributes' => [
              'class' => ['proximity-origin-summary'],
            ],
          ];
        }
      }
    }

  }

  /**
   * Builds the map array
   *
   * @param array $form
   *   The form element to build.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The map picker prepared to be rendered.
   */
  protected function mapPicker(array &$form, FormStateInterface $form_state){

    // Set the defaults.
    $default_lat = $this->origin['lat'];
    $default_lon = $this->origin['lon'];
    $default_distance = $this->viewHandler->options["value"];
    //$default_units = geofield_radius_options()[$this->viewHandler->options['units']];
    if (
      isset($form_state->getUserInput()[$this->viewHandler->options['id']]['source_configuration']['origin']['lat']) &&
      isset($form_state->getUserInput()[$this->viewHandler->options['id']]['source_configuration']['origin']['lon'])
    ){
      $source_input = $form_state->getUserInput()[$this->viewHandler->options['id']]['source_configuration'];
      $default_lat = round($source_input['origin']['lat'], 2);
      $default_lon = round($source_input['origin']['lon'], 2);
      $default_distance = $form_state->getUserInput()[$this->viewHandler->options['id']]['value'];
    }

    $content = [];
    if (!isset($this->configuration['mapbox_api_key'])){
      return $content;
    }
    $layer_type = 'circle';
    switch ($this->configuration['marker_style']){
    case 'symbol':
      $layer_type = 'symbol';
      break;
    default:
      $layer_type = 'circle';
    }
    $this->map =  [
      'Streets' =>
      [
        'access_token' => $this->configuration['mapbox_api_key'],
        'options' => [
          'container' => 'mapbox-streets-' . $this->viewHandler->options['id'],
          'style' => $this->configuration['style'],
          'zoom' => isset($this->configuration['zoom']) ? $this->configuration['zoom'] : 10,
          'center' => [
            (isset($this->configuration["center"]["lon"])) ? $this->configuration["center"]["lon"]:0,
            (isset($this->configuration["center"]["lat"])) ? $this->configuration["center"]["lat"]:0
          ]
        ],
        'config' =>
        [
          'controls' =>
          [
            'AttributionControl' => [
              'compact' => true
            ],
          ]
        ],
        'layers' =>
        [
          [
            'id' => 'radius_distance',
            'type' => 'circle',
            'source' => [
              'type' => 'geojson',
              'data' => [
                'type' => 'FeatureCollection',
                'features' => [],
              ]
            ],
            'filter' => ['any', [ '==', '$type', 'Point']],
          ],
          [
            'id' => 'points',
            'type' => $layer_type,
            'source' => [
              'type' => 'geojson',
              'data' => [
                'type' => 'FeatureCollection',
                // to be filled by items values
                'features' => [],
              ]
            ],
            'filter' => ['any', [ '==', '$type', 'Point']],
          ],
          [
            'id' => 'polygons',
            'type' => 'fill',
            'paint' => [
              'fill-color' => 'rgba(200, 0, 0, 0.4)',
              'fill-outline-color' => 'rgba(200, 0, 0, 1)',
            ],
            'source' => [
              'type' => 'geojson',
              'data' => [
                'type' => 'FeatureCollection',
                'features' => [],
              ]
            ],
            'filter' => ['any', [ '!=', '$type', 'Point']],
          ],
        ]
      ],
    ];
    if ($this->configuration['marker_style'] === "marker") {
      $this->map['Streets']['layers'][1]['paint'] = [
        'circle-radius' => 0,
      ];
    }
    $this->map['Streets']['layers'][0]['paint'] = [
      'circle-radius' => (float)$default_distance,
      'circle-opacity' => 0.4,
    ];

    if ($layer_type === "symbol") {
      $this->map['Streets']['layers'][0]['layout'] = [
        "icon-image" => ($this->configuration['marker_style'] === "symbol") ? "{icon}-15" : "marker_image",
        "icon-size" => 1,
        "text-field" => "{title}",
        "text-font" => ["Open Sans Semibold", "Arial Unicode MS Bold"],
        "text-offset" => [0, 0.6],
        "text-anchor" => "top"
      ];
    }

    // Settings passed to JS picker behavior.
    // The used marker image (none by now as it will use native marker)
    $content['#attached']['drupalSettings']['mapbox_gl_location_picker']["views_filter_" . $this->viewHandler->options["id"]]['marker_image'] = "";
    // The used marker style.
    $content['#attached']['drupalSettings']['mapbox_gl_location_picker']["views_filter_" . $this->viewHandler->options["id"]]['marker_style'] = $this->configuration['marker_style'];
    // The picker type : views_filter, field ...
    $content['#attached']['drupalSettings']['mapbox_gl_location_picker']["views_filter_" . $this->viewHandler->options["id"]]['picker_type'] = 'views_filter';
    // The views handler identifier
    $content['#attached']['drupalSettings']['mapbox_gl_location_picker']["views_filter_" . $this->viewHandler->options["id"]]['views_handler_id']= $this->viewHandler->options["id"];
    // The map where this picker will be applied
    $content['#attached']['drupalSettings']['mapbox_gl_location_picker']["views_filter_" . $this->viewHandler->options["id"]]['map_id']= $this->map['Streets']['options']['container'];
    // The conversion factor
    $content['#attached']['drupalSettings']['mapbox_gl_location_picker']["views_filter_" . $this->viewHandler->options["id"]]['metric_conversion_factor'] = GEOFIELD_METERS/constant($this->viewHandler->options["units"]);


    $items = $this->getItems(["lat" => $default_lat, "lon" => $default_lon]);
    foreach ($items as $delta => $item) {
      $this->addSimpleFeature($item, $delta);
      // adding radius distance layer
      $this->addSimpleFeature($item, $delta, 0);
    }
    $map_info = $this->map['Streets'];
    $content['mapbox-container'] = [
      '#prefix' => '<div class="mapbox-gl-wrapper mapbox-widget">',
      '#markup' => '<div id="'  .  $map_info['options']['container'] . '-menu" class="mapbox-gl-layer-menu"></div>'
      . '<div id="' .  $map_info['options']['container'] . '" class="mapbox-gl-container"></div>',
      '#suffix' => '</div">'
    ];

    $sources = key_exists('sources', $map_info) ? $map_info['sources']: [];
    $layers =  key_exists('layers', $map_info) ? $map_info['layers']: [];

    $settings[$map_info['options']['container']] = [
      'accessToken' => $map_info['access_token'],
      'options' => $map_info['options'],
      'sources' => $sources,
      'layers' => $layers,
      'config' => $map_info['config']
    ];

    global $base_url;
    $content['#attached']['drupalSettings']['baseUrl'] = $base_url;
    $content['#attached']['drupalSettings']['mapboxGl'] = $settings;
    $content['#attached']['library'][] = 'mapbox_gl/libraries.mapbox-gl-js';
    $content['#attached']['library'][] = 'mapbox_gl/mapbox_gl';
    $content['#attached']['library'][] = 'mapbox_gl/libraries.mapbox-gl-geocoder';
    $content['#attached']['library'][] = 'mapbox_gl_location_picker/picker';
    return $content;
  }

  /**
   * Gets the items from the argument or from configuration if any.
   *
   * @param array $in_vals
   *   The [lat lon] values
   * @return array
   *   The items in array prepared to be included as feature to
   *   ::addSimpleFeature()
   */
  protected function getItems($in_vals){
    $items = [];
    $lat = $in_vals['lat'];
    $lon = $in_vals['lon'];
    $items[] = [
      "value" => "POINT ($lon $lat)",
    ];
    return $items;
  }
}

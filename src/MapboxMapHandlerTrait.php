<?php
namespace Drupal\mapbox_gl_location_picker;

use Drupal\geofield\GeoPHP\GeoPHPInterface;

/**
 * Class MapboxMapHandlerTrait
 *
 * Provide functions for Mapbox map array handling.
 *
 * @package Drupal\mapbox_gl_location_picker
 */
trait MapboxMapHandlerTrait {

    /**
     * Appends a feature point from geofield item to the map
     *
     * @param array $item
     *   The item with its properties:
     *     - value : a value that can be converted to lat,lon via geophp
     *     - [title] : a title.
     *     - [description] : a description.
     * @param int                                   $delta
     *   The item delta in the fielditemlist
     * @param int                                 $source
     *   Optionally add to this source id appendng this source to the layer
     */
    protected function addSimpleFeature(array $item, int $delta, int $source = null)
    {
        $geoPhpWrapper = \Drupal::service('geofield.geophp');
				$geom = $geoPhpWrapper->load($item['value']);
        if ($geom && $geom->getGeomType() == 'Point') {
          // if is not set set the center to this feature
		  // lon is 0 key
          if ($this->map['Streets']['options']['center'][0] === 0) {
            /* @var \Point $geom */
            $this->map['Streets']['options']['center'] = [$geom->x(), $geom->y()];
          }
					$feature = [
						'type' => 'Feature',
						'geometry' => [
							'type' => 'Point',
							'coordinates' =>  [ $geom->x(), $geom->y()]
						],
						'properties' => [
							'title' => (isset($item['title']))?$item['title'] . " #" . $delta : NULL,
							'description' => (isset($item['description']))? $item['description'] : NULL,
							'geourl' => "geo:" . $geom->x() . "," . $geom->y(),
						]
					];

					if (is_null($source)) {
						$this->map['Streets']['layers'][1]['source']['data']['features'][] = $feature;
					}
					else {
						$this->map['Streets']['layers'][$source]['source']['data']['features'][] = $feature;
					}
				}
				if ($geom && $geom->getGeomType() == 'Polygon') {
					$this->map['Streets']['options']['center'] = [$geom->getCentroid()->getX(), $geom->getCentroid()->getY()];
					$feature = [
						'type' => 'Feature',
						'geometry' => [
							'type' => 'Polygon',
							'coordinates' =>  $geom->asArray()
						],
					];

					if (is_null($source)) {
						$this->map['Streets']['layers'][2]['source']['data']['features'][] = $feature;
					}
					else {
						$this->map['Streets']['layers'][$source]['source']['data']['features'][] = $feature;
					}
				}
		}


}
